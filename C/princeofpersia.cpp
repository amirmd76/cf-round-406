#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
//#define L(x) ((x)<<1)
//#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 1e5 + 100, maxl = 40;
int a[maxn], nx[maxn], pr[maxn], nxt = 1, root[maxn], n;
vi v[maxn];
int s[maxn * maxl], L[maxn * maxl], R[maxn * maxl];
inline void build(int id = 0, int l = 0, int r = n){
	if(r - l < 2){
		s[id] = pr[l] == -1;
		return ;
	}
	L[id] = nxt ++;
	R[id] = nxt ++;
	int mid = (l + r) >> 1;
	build(L[id], l, mid);
	build(R[id], mid, r);
	s[id] = s[L[id]] + s[R[id]];
}
inline int upd(int p, int val, int id, int l = 0, int r = n){
	int ID = nxt ++;
	s[ID] = s[id] + val;
	L[ID] = L[id];
	R[ID] = R[id];
	if(r - l < 2)	return ID;
	int mid = (l + r) >> 1;
	if(p < mid)
		L[ID] = upd(p, val, L[id], l, mid);
	else
		R[ID] = upd(p, val, R[id], mid, r);
	return ID;
}
inline int kth(int k, int id, int l = 0, int r = n){
	if(s[id] < k)	return n;
	if(r - l < 2)	return l;
	int mid = (l + r) >> 1;
	if(s[L[id]] >= k)
		return kth(k, L[id], l, mid);
	else
		return kth(k - s[L[id]], R[id], mid, r);
}
int main(){
	memset(nx, -1, sizeof nx);
	memset(pr, -1, sizeof pr);
	scanf("%d", &n);
	For(i,0,n){
		scanf("%d", a+i);
		-- a[i];
		v[a[i]].pb(i);
	}
	For(i,0,n)
		For(j,1,(int)v[i].size())
			nx[v[i][j-1]] = v[i][j],
			pr[v[i][j]] = v[i][j-1];
	build();
	root[0] = 0;
	For(i,1,n){
		int cur = root[i-1];
		cur = upd(i-1, -1, cur);
		if(~nx[i-1])
			cur = upd(nx[i-1], 1, cur);
		root[i] = cur;
	}
	For(k,1,n+1){
		int ans = 0;
		if(k == n)
			ans = 1;
		else{
			int cur = 0;
			while(cur < n){
				++ ans;
				cur = kth(k+1, root[cur]);
			}
		}
		printf("%d ", ans);
	}
	puts("");
	return 0;
}
