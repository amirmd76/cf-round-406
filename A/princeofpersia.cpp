#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 7000 + 10;
int n;
int mod(int x){
	while(x >= n)	x -= n;
	return x;
}
vi v[2];
int dp[maxn][2], deg[maxn][2]; // 1 for WIN and 2 for LOSE
void go(int i, int j){
	int op = !j;
	rep(x, v[op]){
		int p = mod(i + n - x);
		if(dp[p][op])	continue ;
		if(dp[i][j] == 1){
			-- deg[p][op];
			if(!deg[p][op]){
				dp[p][op] = 2;
				go(p, op);
			}
		}
		else{
			dp[p][op] = 1;
			go(p, op);
		}
	}
}
int main(){
	iOS;
	cin >> n;
	For(i,0,2){
		int k;
		cin >> k;
		while(k--){
			int a;
			cin >> a;
			v[i].pb(a);
		}
	}
	For(i,0,n)
		For(j,0,2)	deg[i][j] = (int)v[j].size();
	dp[0][0] = dp[0][1] = 2;
	go(0, 0);
	go(0, 1);
	For(j,0,2)
		For(i,1,n)
			cout << (dp[i][j]? dp[i][j] == 1? "Win": "Lose": "Loop") << " \n"[i + 1 == n];
	return 0;
}
