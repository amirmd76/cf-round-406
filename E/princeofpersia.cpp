#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef pair<vi, vi> pvv;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 2e4 + 100, maxl = 15;
template <class FlowT> class MaxFlow{
	public:
		static const int maxn = 4e5+100, maxm = 1e6+100;
		static const FlowT FlowEPS = FlowT(1e-8), FlowINF = FlowT(1 << 29);
		int to[maxm * 2], prv[maxm * 2], hed[maxn], dis[maxn], pos[maxn];
		int mat[maxn][2], stat[maxn], assignee[maxn];
		bool mark[maxn];
		FlowT cap[maxm * 2];
		int n, m;
		inline void init(int N){
			n = N, m = 0;
			memset(hed, -1, n * sizeof hed[0]);
			memset(stat, -1, sizeof mat); 
			memset(mat, -1, sizeof mat);
			memset(assignee, -1, sizeof assignee);
		}
	private:
		inline void add_single_edge(int v, int u, FlowT c){
			assert(m < 2*maxm);
			to[m] = u, prv[m] = hed[v], cap[m] = c, hed[v] = m ++;
		}
	public:
		void set_ver(int v, int s, int a){
			stat[v] = s, assignee[v] = a;
		}
		inline void add_edge(int v, int u, FlowT c){
			add_single_edge(v, u, c);
			add_single_edge(u, v, 0);
		}
		inline bool bfs(int source, int sink){
			static int qu[maxn], head, tail;
			head = tail = 0;
			memset(dis, -1, n * sizeof dis[0]);
			dis[source] = 0;
			qu[tail ++] = source;
			while(head < tail){
				int v = qu[head ++];
				for(int e = hed[v]; e + 1; e = prv[e])
					if(cap[e] > FlowEPS && dis[to[e]] == -1)
						dis[to[e]] = dis[v] + 1, qu[tail ++] = to[e];
				if(dis[sink] + 1)	break ;
			}
			return dis[sink] + 1;
		}
		inline FlowT dfs(int v, int sink, FlowT cur = FlowINF){
			if(v == sink)	return cur;
			FlowT ans = 0;
			for(int &e = pos[v]; e + 1; e = prv[e])
				if(cap[e] > FlowEPS && dis[to[e]] == dis[v] + 1){
					FlowT tmp = dfs(to[e], sink, min(cur, cap[e]));
					cur -= tmp;
					ans += tmp;
					cap[e] -= tmp;
					cap[e ^ 1] += tmp;
					if(cur <= FlowEPS/2)	break ;
				}
			return ans;
		}
		inline FlowT flow(int source, int sink){
			FlowT ans = 0;
			while(bfs(source, sink)){
				memcpy(pos, hed, n * sizeof hed[0]);
				ans += dfs(source, sink);
			}
			return ans;
		}
		inline int dfs(int v){
			if(stat[v] == 1)	return v;
			for(int &e = pos[v]; e + 1; e = prv[e]){
				if((e & 1) or !cap[e ^ 1])	continue ;
				-- cap[e ^ 1];
				++ cap[e];
				return dfs(to[e]);
			}
			return -1;
		}
		inline void find_matches(){
			memcpy(pos, hed, n * sizeof hed[0]);
			For(v,0,n)	if(!stat[v]){
				int u = dfs(v);
				if(~u)
					mat[v][0] = u, mat[u][1] = v;
			}
		}
		inline void DFS(int v){
			mark[v] = true;
			if(stat[v] == 1){
				if(~mat[v][1])
					DFS(mat[v][1]);
				return ;
			}
			for(int e = hed[v]; e + 1; e = prv[e]) if(!(e & 1)){
				int u = to[e];
				if(!mark[u])	DFS(u);
			}

		}
		inline pvv vertex_cover(){ // (top, bottom) = (queries, edges)
			find_matches();
			fill(mark, mark + maxn, false);
			For(v,0,n)	if(!stat[v] && mat[v][0] == -1)
				DFS(v);
			vi q, e;
			For(v,0,n){
				if(!stat[v] && ~mat[v][0] && !mark[mat[v][0]])
					q.pb(assignee[v]);
				else if(stat[v] == 1 && mark[v]){
					assert(~mat[v][1]);
					e.pb(assignee[v]);
				}
			}
			return pvv(q, e);
		}
};
MaxFlow<int> F;
const int N = 4e5;
const int S = 0, T = 1;
int nx = 2;
vector<pii> adj[maxn];
int par[maxn][maxl], inp[maxn][maxl];
int h[maxn];
inline void dfs(int v = 0, int p = -1, int ep = -1){
	par[v][0] = p;
	if(~p){
		h[v] = h[p] + 1;
		inp[v][0] = nx;
		F.set_ver(nx, 1, ep);
		F.add_edge(nx ++, T, 1);
	}
	For(i,1,maxl)	if(~par[v][i-1]){
		par[v][i] = par[par[v][i-1]][i-1];
		if(~par[v][i]){
			inp[v][i] = nx;
			F.add_edge(nx, inp[v][i-1], N);
			F.add_edge(nx ++, inp[par[v][i-1]][i-1], N);
		}
	}
	rep(ue, adj[v]){
		int u = ue.x, e = ue.y;
		if(u - p)
			dfs(u, v, e);
	}
}
inline int lca(int v, int u, int ver){
	if(h[v] < h[u])	swap(v, u);
	rof(i,maxl-1,-1)	if(~par[v][i] && h[par[v][i]] >= h[u]){
		F.add_edge(ver, inp[v][i], N);
		v = par[v][i];
	}
	if(v == u)	return v;
	rof(i,maxl-1,-1)	if(par[v][i] != par[u][i]){
		F.add_edge(ver, inp[v][i], N);
		F.add_edge(ver, inp[u][i], N);
		v = par[v][i], u = par[u][i];
	}
	F.add_edge(ver, inp[v][0], N);
	F.add_edge(ver, inp[u][0], N);
	return par[v][0];
}
int main(){
	iOS;
	memset(par, -1, sizeof par);
	int n, m;
	cin >> n >> m;
	F.init(N);
	For(i,1,n){
		int v, u;
		cin >> v >> u;
		-- v, -- u;
		adj[v].pb({u, i});
		adj[u].pb({v, i});
	}
	dfs();
	For(i,0,m){
		int v, u;
		cin >> v >> u;
		-- v, -- u;
		int ver = nx;
		F.set_ver(nx, 0, i + 1);
		F.add_edge(S, nx++, 1);
		lca(v, u, ver);
	}
	assert(nx < N);
	cout << F.flow(S, T) << '\n';
	pvv pv = F.vertex_cover();
	vi vc[2] = {pv.x, pv.y};
	For(i,0,2){
		cout << vc[i].size() << ' ';
		rep(v, vc[i])
			cout << v << ' ';
		cout << '\n';
	}
	return 0;
}
