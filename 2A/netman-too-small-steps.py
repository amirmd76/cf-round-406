a, b = map(int, input().split(' '))

c, d = map(int, input().split(' '))

x = [b + i * a for i in range(100)]
y = [d + i * c for i in range(100)]

res = -1

for t in x:
    if t in y:
        res = t
        break

print(res)