n, m = [int(i) for i in input().split()]

for i in range(m):
    a = set([j for j in input().split()][1:])
    f = True
    for x in a:
        if str(-int(x)) in a:
            f = False
    if f:
        print("YES")
        exit()
print("NO")