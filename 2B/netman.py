#!/usr/bin/python3.5

n, m = map(int, input().split(' '))

mark = [0] * n

res = False

for x in range(m):
    seq = list(map(int, input().split(' ')))
    seq = seq[1:]
    seq = list(set(seq))
    seq = list(map(abs, seq))
    if len(seq) == len(set(seq)):
       res = True

if res:
    print('YES')
else:
    print('NO')
