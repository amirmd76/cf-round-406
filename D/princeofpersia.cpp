#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 2e4 + 100, maxl = 15;
int mod[2] = {(int)1e9 + 7, (int)1e9 + 9}, base[2] = {717, 749};
int pw[2][maxn], centroid;
int par[maxn][maxl], up[maxn][maxl][2], dw[maxn][maxl][2], h[maxn], p2[maxl];
typedef pair<int, char> edge;
int n, q;
pii query[maxn];
vi queries[maxn];
int ANS[maxn];
vector<edge> adj[maxn];
char c2p[maxn];
bool block[maxn];
int sz[maxn];
inline void dfs(int v = 0, int p = -1, char ch = -1){
	if(~p){
		h[v] = h[p] + 1;
		c2p[v] = ch;
		For(e,0,2)
			up[v][0][e] = dw[v][0][e] = c2p[v] - 'a';
		par[v][0] = p;
	}
	For(i,1,maxl){
		if(~par[v][i-1])	par[v][i] = par[par[v][i-1]][i-1];
		if(~par[v][i])
			For(e,0,2){
				up[v][i][e] = (1LL * up[v][i-1][e] * pw[e][p2[i-1]] + up[par[v][i-1]][i-1][e]) % mod[e];
				dw[v][i][e] = (1LL * dw[par[v][i-1]][i-1][e] * pw[e][p2[i-1]] + dw[v][i-1][e]) % mod[e];
			}
	}
	rep(e, adj[v]){
		int u = e.x;
		if(u - p)	dfs(u, v, e.y);
	}
}
inline int lca(int v, int u){
	if(h[v] < h[u])	swap(v, u);
	rof(i,maxl-1,-1)	if(~par[v][i] && h[par[v][i]] >= h[u])
		v = par[v][i];
	if(v == u)	return v;
	rof(i,maxl-1,-1)	if(par[v][i] - par[u][i])
		v = par[v][i], u = par[u][i];
	return par[v][0];
}
inline int dist(int v, int u){
	return h[v] + h[u] - 2 * h[lca(v, u)];
}
inline int kth_anc(int v, int k){
	rof(i,maxl-1,-1) if(k >= p2[i]){
		k -= p2[i];
		v = par[v][i];
		assert(~v);
	}
	return v;
}
inline int kth_on_path(int v, int u, int k){
	int x = lca(v, u);
	int a = h[v] - h[x],
		b = h[u] - h[x];
	assert(a + b >= k);
	if(a >= k)
		return kth_anc(v, k);
	return kth_anc(u, a + b - k);
}
inline char kth_edge_on_path(int v, int u, int k){
	int x = lca(v, u);
	int a = h[v] - h[x],
		b = h[u] - h[x];
	assert(a + b >= k);
	if(a >= k)
		return c2p[kth_anc(v, k-1)];
	return c2p[kth_anc(u, a + b - k)];
}
inline pii hash_up(int v, int u){
	int ans[2] = {};
	rof(i,maxl-1,-1)	if(~par[v][i] && h[par[v][i]] >= h[u]){
		For(e,0,2)
			ans[e] = (1LL * ans[e] * pw[e][p2[i]] + up[v][i][e]) % mod[e];
		v = par[v][i];
	}
	return {ans[0], ans[1]};
}
inline pii hash_dw(int v, int u){
	int ans[2] = {};
	int cur = 0;
	rof(i,maxl-1,-1)	if(~par[v][i] && h[par[v][i]] >= h[u]){
		For(e,0,2)
			ans[e] = (ans[e] + 1LL * pw[e][cur] * dw[v][i][e]) % mod[e];
		cur += p2[i];
		v = par[v][i];
	}
	return {ans[0], ans[1]};
}
inline pii hash_path(int v, int u){
	int x = lca(v, u);
	int a = h[v] - h[x],
		b = h[u] - h[x];
	pii U = hash_up(v, x), D = hash_dw(u, x);
	return {(1LL * U.x * pw[0][b] + D.x) % mod[0],	(1LL * U.y * pw[1][b] + D.y) % mod[1]};
}
bool olcp;
inline int lcp(int v, int u, int x, int y){
	int d1 = dist(v, u), d2 = dist(x, y);
	int r = 1 + min(d1, d2), l = 0;
	while(l + 1 < r){
		int mid = (l + r) >> 1;
		int w = kth_on_path(v, u, mid),
			z = kth_on_path(x, y, mid);
		if(hash_path(v, w) == hash_path(x, z))
			l = mid;
		else
			r = mid;
	}
	return l;
}
inline int cmp(int v, int u, int x, int y){ // first: neg, second: pos
	olcp = true;
	if(v == u && x == y)	return 0;
	if(x == y)	return 1;
	if(v == u)	return -1;
	olcp = false;
	int d1 = dist(v, u), d2 = dist(x, y);
	int l = lcp(v, u, x, y);
	if(l == d1 || l == d2)	olcp = true;
	if(d1 == d2 && l == d1)	return 0;
	if(l == d1)	return -1;
	if(l == d2)	return 1;
	int c1 = kth_edge_on_path(v, u, l+1) - 'a',
		c2 = kth_edge_on_path(x, y, l+1) - 'a';
	assert(c1 != c2);
	return c1 - c2;
}
int thash[maxn * 2][2], tri[maxn * 2][27], nx = 1;
typedef pair<pii, int> piii;
vector<piii > mp;
vi weight[maxn * 2];
vector<pii> qth[maxn * 2];
int sig[maxn], tot;
inline void clean(){
	For(i,0,nx){
		thash[i][0] = thash[i][1] = 0;
		For(j,0,27)	tri[i][j] = 0;
		rep(w, weight[i])	if(~w)	sig[w] = 0;
		weight[i].clear();
		qth[i].clear();
	}
	tot = 0;
	nx = 1;
	mp.clear();
}
inline void dfs_on_trie(int v = 0){
	rep(qt, qth[v]){
		int q = qt.x, afc = qt.y;
		int ans = tot;
		if(~afc)
			ans -= sig[afc];
		ANS[q] += ans;
	}
	rep(w, weight[v]){
		++ tot;
		if(~w)	++ sig[w];
	}
	For(i,0,27)	if(tri[v][i])	dfs_on_trie(tri[v][i]);
}
inline int MP(pii p){
	pair<pii, int> P = make_pair(p, -1);
	lower_bound(all(mp), P);
	int x = lower_bound(all(mp), make_pair(p, -1)) - mp.begin();
	if(x == (int)mp.size())	return -1;
	if(mp[x].x != p)	return -1;
	return mp[x].y;
}
inline void insert(int q, int afc){
	int ov = query[q].x, u = query[q].y;
	int v = kth_on_path(ov, u, dist(ov, centroid));
	int d = dist(v, u);
	int l = 0, r = d + 1;
	while(l + 1 < r){
		int mid = (l + r) >> 1;
		int w = kth_on_path(v, u, mid);
		if(~MP(hash_path(v, w)))
			l = mid;
		else
			r = mid;
	}
	int w = kth_on_path(v, u, l);
	if(u == w)
		qth[MP(hash_path(v, w))].pb({q, afc});
	else{
		int cur = MP(hash_path(v, w));
		assert(~cur);
		int c = kth_edge_on_path(v, u, l+1) - 'a';
		if(!tri[cur][c])
			tri[cur][c] = nx ++;
		cur = tri[cur][c];
		qth[cur].pb({q, afc});
	}
}
inline void add_to_trie(int v, int c){
	tri[v][c] = nx ++;
	int u = tri[v][c];
	For(e,0,2)
		thash[u][e] = (1LL * thash[v][e] * base[e] + c) % mod[e];
	pii p = {thash[u][0], thash[u][1]};
	mp.pb(piii(p, u));
	piii ww = mp[0];
}
inline void dfs_add(int v, int p = -1, int cur = 0, int afc = -1){
	weight[cur].pb(afc);
	rep(e, adj[v]){
		int u = e.x, c = e.y - 'a';
		if(block[u] or u == p)	continue;
		if(!tri[cur][c])	add_to_trie(cur, c);
		dfs_add(u, v, tri[cur][c], (v == centroid? u: afc));
	}
}
inline void dfs_insert(int v, int p = -1, int afc = -1){
	rep(q, queries[v]){
		int u = query[q].y;
			int res = cmp(v, u, v, centroid);
		if(res < 0)	continue ;
		if(res && !olcp)	ANS[q] += sz[centroid] - (~afc? sz[afc]: 1);
		else
			insert(q, afc);
	}
	rep(e, adj[v]){
		int u = e.x;
		if(block[u] or u == p)	continue;
		dfs_insert(u, v, (v == centroid? u: afc));
	}
}
inline int DFS(int v, int p = -1){
	sz[v] = 1;
	rep(e, adj[v]){
		int u = e.x;
		if(u - p && !block[u])	sz[v] += DFS(u, v);
	}
	return sz[v];
}
inline void centroid_decomposition(int v = 0){
	int N = DFS(v), prv = -1;
	bool fnd = true;
	while(fnd){
		fnd = false;
		rep(e, adj[v]){
			int u = e.x;
			if(!block[u] && u != prv && sz[u] * 2 >= N){
				prv = v;
				v = u;
				fnd = true;
				break ;
			}
		}
	}
	block[v] = true;
	rep(e, adj[v]){
		int u = e.x;
		if(!block[u])	centroid_decomposition(u);
	}
	centroid = v;
	DFS(v);
	dfs_add(v);
	mp.pb({{0, 0}, 0});
	sort(all(mp));
	mp.resize(unique(all(mp)) - mp.begin());
	dfs_insert(v);
	dfs_on_trie();
	clean();
	block[v] = false;
}
int main(){
	memset(par, -1, sizeof par);
	For(i,0,maxl)	p2[i] = 1 << i;
	For(e,0,2){
		pw[e][0] = 1;
		For(i,1,maxn)
			pw[e][i] = (1LL * pw[e][i-1] * base[e]) % mod[e];
	}
	iOS;
	cin >> n >> q;
	For(i,1,n){
		int v, u;
		char c;
		cin >> v >> u >> c;
		++ c;
		-- v, -- u;
		adj[v].pb({u, c});
		adj[u].pb({v, c});
	}
	For(i,0,q){
		int v, u;
		cin >> v >> u;
		-- v, -- u;
		query[i] = {v, u};
		queries[v].pb(i);
	}
	dfs();
	centroid_decomposition();
	For(i,0,q)
		cout << -- ANS[i] << '\n';
	return 0;
}
